﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TelegBot
{
    class Program
    {
        private static Dictionary<string , string > DataBase;
        static void Main(string[] args)
        {
            var Data = File.ReadAllText("C:/Users/1/source/repos/TelegBot/TelegBot/Database/Database.json");
            DataBase = JsonConvert.DeserializeObject<Dictionary<string, string>>(Data);

            var Api = new TelegramAPI();
            while (true)
            {
               var Updates =  Api.getUpdates();
                foreach (var update in Updates)
                {  
                    var answer = answerQuestion(update.message.text);
                    var message = $"Dear,{update.message.chat.first_name},{answer}";
                    Api.sendMessage(message, update.message.chat.id);
                }

            }
            
            
            
        }
        private static string answerQuestion(string Question)
        {
            var UserQuestion = Question.ToLower();
            var Answers = new List<string>();

            foreach (var Entry in DataBase)
            {
                if (UserQuestion.Contains(Entry.Key))
                {
                    Answers.Add(Entry.Value);
                }
            }
            if (Answers.Count==0)
            {
                Answers.Add("ты что хочешь?");
            }
            var Result = String.Join(",", Answers);
            return Result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;

namespace TelegBot
{
    public class TelegramAPI
    {
        public class ApiResult
        {
            public Update[] result { get; set; }
        }

        public class Update
        {
            public int update_id { get; set; }
            public Message message { get; set; }

        }

        public class Message
        {
            public Chat chat { get; set; }
            public string text { get; set; }
        }

        public class Chat
        {
            public int id { get; set; }
            public string first_name { get; set; }
        }

        private int lastUpdateId = 0;
        public TelegramAPI() { }
        
        RestClient RC = new RestClient();

        const string API_URL = "https://api.telegram.org/bot" + SecretKey.API_KEY + "/";

        //sendMessage 
        //getUpdate

        public void sendMessage(string text, int chat_id)
        {
            sendAPIRequest("sendMessage", $"chat_id={chat_id}&text={text}");
        }

        public Update[] getUpdates()
        {
            var json = sendAPIRequest("getUpdates", $"offset={lastUpdateId}");
            var ApiResult = JsonConvert.DeserializeObject<ApiResult>(json);
            foreach (var update in ApiResult.result)
            {
                Console.WriteLine($"получен апдейт {update.update_id},"
                    + $"сообщение от {update.message.chat.first_name},"
                    + $"текст {update.message.text}");
                lastUpdateId = update.update_id + 1;
            }
            return ApiResult.result;
        }
     
        private string sendAPIRequest(string ApiMethod, string Params)
            {
                
                 // делаем готовый запрос
                var Url = API_URL + ApiMethod + "?" + Params;
                //готовим объект запроса
                var Request = new RestRequest(Url);
                //выполняем запрос
                var Response = RC.Get(Request);
                //возвращаем результат
                return Response.Content;
            }
    }
}
